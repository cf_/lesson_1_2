
//
//  CalculatorBrain.swift
//  Calculator
//
//  Created by Arseny Ivanov on 11/11/16.
//  Copyright © 2016 JSON LLC. All rights reserved.
//

import Foundation

func multiply(op1: Double, op2: Double) -> Double {
    return op1 * op2
}

class CalculatorBrain
{
    
    private var accumulator = 0.0
    
    func setOperand(operand: Double) {
        accumulator = operand
    }
    
    private var pending: PendingBinaryOperationInfo?
    
    private struct PendingBinaryOperationInfo {
        var binaryFunction: (Double, Double) -> Double
        var firstOperand: Double
    }
    
    private var operations: Dictionary<String, Operations> = [
        "π":    .Constant(M_PI), // M_PI
        "e":    .Constant(M_E), // M_E
        
        "±":    .UnaryOperation({ -$0 }),
        "√":    .UnaryOperation(sqrt), // sqrt
        "cos":  .UnaryOperation(cos), // cos
        
        "×":    .BinaryOperation({ $0 * $1 }),
        "÷":    .BinaryOperation({ $0 / $1 }),
        "+":    .BinaryOperation({ $0 + $1 }),
        "-":    .BinaryOperation({ $0 - $1 }),
        "=":    .Equals
    ]
    
    private enum Operations {
        case Constant(Double)
        case UnaryOperation((Double) -> Double)
        case BinaryOperation((Double, Double) -> Double)
        case Equals
    }
    
    func performOperation(symbol: String) {
        if let operation = operations[symbol] {
            switch operation {
            case .Constant (let value):
                accumulator = value
            case .UnaryOperation (let function):
                accumulator = function(accumulator)
            case .BinaryOperation (let function):
                executePendingBinaryOperation()
                pending = PendingBinaryOperationInfo(binaryFunction: function, firstOperand: accumulator)
            case .Equals:
                executePendingBinaryOperation()
            }
        }
    }
    
    private func executePendingBinaryOperation() {
        if pending != nil {
            accumulator = pending!.binaryFunction(pending!.firstOperand, accumulator)
            pending = nil
        }
    }
    
    var result: Double {
        get {
            return accumulator
        }
    }
}
